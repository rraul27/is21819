package es.unican.is2.banco;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Credito extends Tarjeta {

	private double mCredito;
	private List<Movimiento> mMovimientosMensuales;
	private List<Movimiento> historicoMovimientos;

	private double saldo;

	public Credito(String numero, String titular, double credito) {
		super(numero, titular);
		mCredito = credito;
		mMovimientosMensuales = new ArrayList<Movimiento>();
		historicoMovimientos = new ArrayList<Movimiento>();
		saldo = 0;
	}

	@Override
	public void ingresar(double x) throws DatoErroneoException{
		if (x <= 0)
			throw new DatoErroneoException("No se puede ingresar una cantidad negativa");

		Movimiento m = new Movimiento();
		Date d = new Date();
		m.setFecha(d);
		m.setConcepto("Ingreso en cajero autom�tico");
		m.setImporte(x);
		mMovimientosMensuales.add(m);
		saldo +=x;
	}

	@Override
	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (x<0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");

		Movimiento m = new Movimiento();
		Date d = new Date();
		m.setFecha(d);
		m.setConcepto("Retirada en cajero autom�tico");
		x += x * 0.05; // A�adimos una comisi�n de un 5%
		m.setImporte(-x);

		if (x > getSaldo()+mCredito)
			throw new SaldoInsuficienteException("Cr�dito insuficiente");
		else {
			mMovimientosMensuales.add(m);
			saldo -=x;
		}
	}

	@Override
	public void pagoEnEstablecimiento(String datos, double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (x<0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");
		Date d = new Date();
		if (x > getSaldo()+mCredito)
			throw new SaldoInsuficienteException("Saldo insuficiente");

		Movimiento m = new Movimiento();
		m.setConcepto("Compra a cr�dito en: " + datos);
		m.setImporte(-x);
		mMovimientosMensuales.add(m);
		saldo = saldo -x;
	}

	@Override
	public double getSaldo() {
		return saldo;
	}


	public Date getCaducidadCredito() {
		return this.mCuentaAsociada.getCaducidadCredito();
	}

	/**
	 * M�todo que se invoca autom�ticamente el d�a 1 de cada mes
	 */
	public void liquidar() {
		Movimiento liq = new Movimiento();
		liq.setConcepto("Liquidaci�n de operaciones tarj. cr�dito");
		double r = 0.0;
		for (int i = 0; i < this.mMovimientosMensuales.size(); i++) {
			Movimiento m = (Movimiento) mMovimientosMensuales.get(i);
			r += m.getImporte();
		}
		liq.setImporte(r);

		if (r != 0)
			mCuentaAsociada.addMovimiento(liq);

		historicoMovimientos.addAll(mMovimientosMensuales);
		mMovimientosMensuales.clear();
		saldo -= r;
	}

	public List<Movimiento> getMovimientosUltimoMes() {
		return mMovimientosMensuales;
	}

	public Cuenta getCuentaAsociada() {
		return mCuentaAsociada;
	}

	public List<Movimiento> getMovimientos() {
		return mMovimientosMensuales;
	}

}