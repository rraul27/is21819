package es.unican.is2.banco;

import java.util.Date;

public abstract class Tarjeta {
	protected String mNumero, mTitular;
		
	protected Cuenta mCuentaAsociada;

	public Tarjeta(String numero, String titular) {
		mNumero = numero;
		mTitular = titular;	
	}

	public void setCuenta(Cuenta c) {
		mCuentaAsociada = c;
	}

	public abstract void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException;

	public abstract void pagoEnEstablecimiento(String datos, double x)
			throws SaldoInsuficienteException, DatoErroneoException;

	public abstract double getSaldo();
	
	public abstract void ingresar(double x) throws DatoErroneoException;
}