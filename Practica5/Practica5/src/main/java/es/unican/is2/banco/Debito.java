package es.unican.is2.banco;

import java.util.Date;

public class Debito extends Tarjeta {
	
	private double saldoDiarioDisponible;

	public Debito(String numero, String titular, Date fechaCaducidad) {
		super(numero, titular);
	}
	
	@Override
	public void ingresar(double x) throws DatoErroneoException{	
		this.mCuentaAsociada.ingresar("Ingreso en cajero autom�tico",x);
		saldoDiarioDisponible+=x;

	}

	@Override
	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (saldoDiarioDisponible<x) {
			throw new SaldoInsuficienteException("Saldo insuficiente");
		}
		this.mCuentaAsociada.retirar("Retirada en cajero autom�tico", x);
		saldoDiarioDisponible-=x;
	}
	
	@Override
	public void pagoEnEstablecimiento(String datos, double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (saldoDiarioDisponible<x) {
			throw new SaldoInsuficienteException("Saldo insuficiente");
		}
		this.mCuentaAsociada.retirar("Compra en : " + datos, x);
		saldoDiarioDisponible-=x;
	}

	@Override
	public double getSaldo() {
		return saldoDiarioDisponible;
	}
	
	public Date getCaducidadDebito() {
		return this.mCuentaAsociada.getCaducidadDebito();
	}
	
	/**
	 * M�todo invocado autom�ticamente a las 00:00 de cada d�a
	 */
	public void restableceSaldo() {
		saldoDiarioDisponible = mCuentaAsociada.getLimiteDebito();
	}
	
	public Cuenta getCuentaAsociada() {
		return mCuentaAsociada;
	}

}