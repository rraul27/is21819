package es.unican.is2.banco;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;
public class Cuenta {

	private String mNumero;
	//	private String mTitular, mDireccion, mtelefono, mDNI;
	private Cliente cliente;
	private double saldo;
	private List<Movimiento> mMovimientos;
	private Date mFechaDeCaducidadTarjetaDebito;
	private Date mFechaDeCaducidadTarjetaCredito;
	private double limiteDebito;

	public Cuenta(String mNumero, String mTitular, String mDireccion,
			String mtelefono, String mDNI, Date date, Date date2) {

		this.cliente = new Cliente(mTitular,mDireccion,mtelefono,mDNI);
		this.mNumero = mNumero;
		this.mFechaDeCaducidadTarjetaDebito=date;
		this.mFechaDeCaducidadTarjetaCredito=date2;
		mMovimientos=new LinkedList<Movimiento>();
		limiteDebito = 1000;
	}


	public void ingresar(double x) throws DatoErroneoException {
		ingresar("Ingreso en efectivo",x);
	}

	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		retirar("Retirada de efectivo",x);
	}


	public void ingresar(String concepto, double x) throws DatoErroneoException {
		if (x <= 0)
			throw new DatoErroneoException("No se puede ingresar una cantidad negativa");
		Movimiento m = new Movimiento();
		Date d = new Date();
		m.setFecha(d);
		m.setConcepto(concepto);
		m.setImporte(x);
		this.mMovimientos.add(m);
		saldo += x;
	}

	public void retirar(String concepto, double x) throws SaldoInsuficienteException, DatoErroneoException {
		if (getSaldo() < x)
			throw new SaldoInsuficienteException("Saldo insuficiente");
		if (x <= 0)
			throw new DatoErroneoException("No se puede retirar una cantidad negativa");
		Movimiento m = new Movimiento();
		Date d = new Date();
		m.setFecha(d);
		m.setConcepto(concepto);
		m.setImporte(-x);
		this.mMovimientos.add(m);
		saldo -= x;
	}


	public double getSaldo() {
		return saldo;
	}

	public void addMovimiento(Movimiento m) {
		mMovimientos.add(m);
		saldo += m.getImporte();
	}

	public List<Movimiento> getMovimientos() {
		return mMovimientos;
	}

	public Date getCaducidadDebito(){
		return this.mFechaDeCaducidadTarjetaDebito;
	}

	public Date getCaducidadCredito(){
		return this.mFechaDeCaducidadTarjetaCredito;
	}


	public double getLimiteDebito() {
		return limiteDebito;
	}

}