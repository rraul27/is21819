package es.unican.is2.banco;

import static org.junit.Assert.*;
import java.util.Date;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unican.is2.banco.*;

public class CreditoTest {
	static Credito tCredito;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		tCredito = new Credito("43181681321654", "�scar Lopez", 200);
		tCredito.setCuenta(new Cuenta("4846846516", "�scar Lopez", "Castro", "942543674","5984563X",new Date(2015,12,12), new Date(2015,12,12)));
	}// setUpBeforeClass

	@Test
	public void test() {

		// Test getSaldo() y addMovimiento()

		
		// ingresamos
		try {
			tCredito.ingresar(100);				
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(tCredito.getSaldo() == 100);
		assertTrue(tCredito.getMovimientos()
				.get(tCredito.getMovimientos().size() - 1).getConcepto()
				.equals("Ingreso en cajero autom�tico"));

		
		// retiramos
		try {
			tCredito.retirar(150);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(tCredito.getSaldo() == -57.5);
		assertTrue(tCredito.getMovimientos()
				.get(tCredito.getMovimientos().size() - 1).getConcepto()
				.equals("Retirada en cajero autom�tico"));

		
		// retiramos
		try {
			
			tCredito.retirar(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(tCredito.getSaldo() == -62.75);

		
		// pago desde establecimiento
		try {
			tCredito.pagoEnEstablecimiento("Prueba1", 50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(tCredito.getSaldo() == -112.75);
		assertTrue(tCredito.getMovimientos()
				.get(tCredito.getMovimientos().size() - 1).getConcepto()
				.equals("Compra a cr�dito en: Prueba1"));


		
		// retiramos mas del credito disponible
		try {
			tCredito.retirar(500);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals("Cr�dito insuficiente"));
		}
		assertTrue(tCredito.getSaldo() == -112.75);


		
		// ingresamos
		try {
			tCredito.ingresar(612.75);
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue(tCredito.getSaldo() == 500.0);


		
		// liquidar()
		Date hoy = new Date();
		Cuenta cAsociada = tCredito.getCuentaAsociada();

		tCredito.liquidar();
		cAsociada = tCredito.getCuentaAsociada();
		assertTrue(cAsociada.getSaldo() == 500.0);


		
		assertTrue(cAsociada
				.getMovimientos()
				.get(cAsociada.getMovimientos().size() - 1)
				.getConcepto()
				.equals("Liquidaci�n de operaciones tarj. cr�dito"));


		// Ponemos los movimientos de la tarjeta a 0
		try {
			tCredito.retirar(500);
			tCredito.ingresar(25);
		} catch (Exception e) {
			
		}
		assertTrue(tCredito.getSaldo() == 0.0);

		
		tCredito.liquidar();
		cAsociada = tCredito.getCuentaAsociada();
		assertTrue(cAsociada.getSaldo() == 500.0);

		assertTrue(cAsociada.getMovimientos()
				.get(cAsociada.getMovimientos().size() - 1).getImporte() != 0.0);
	}
}// EscenarioCTest
