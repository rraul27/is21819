package es.unican.is2.banco;

import java.util.Date;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unican.is2.banco.*;

public class CuentaTest {
	static Cuenta cuenta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cuenta = new Cuenta("794311", "Juan Gomez", "Torrelavega", "942589674","874214563X",new Date(2015,12,12), new Date(2015,12,12));
	}// setUpBeforeClass

	@Test
	public void test1() {

		// Test getSaldo() y addMovimiento()
		assertTrue(cuenta.getSaldo() == 0);

		Movimiento movimientoa = new Movimiento();
		movimientoa.setImporte(100);

		cuenta.addMovimiento(movimientoa);
		assertTrue(cuenta.getSaldo() == 100);

		// Test retirar()
		try {
			cuenta.retirar(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Retirada de efectivo"));
		assertTrue(cuenta.getSaldo() == 50);

		// Test ingresar negativo
		try {
			cuenta.ingresar(-1);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50);

		// Test ingresar el limite
		try {
			cuenta.ingresar(0.01);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getConcepto()
				.equals("Ingreso en efectivo"));
		assertTrue(cuenta.getSaldo() == 50.01);

		// Test ingresar el limite con concepto
		try {
			cuenta.ingresar("R1", 0.5);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.51);

		// Test retirar el limite con concepto
		try {
			cuenta.retirar("R1", 0.5);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.01);

		// Test ingresar negativo
		try {
			cuenta.retirar(-1);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50.01);

		// Test retirar el limite
		try {
			cuenta.retirar(0.01);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		assertTrue(cuenta.getSaldo() == 50);
	}// test1

	@Test
	public void test2() {
		try {
			cuenta.ingresar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"No se puede ingresar una cantidad negativa"));
		}
	}// test2

	@Test
	public void test3() {
		try {
			cuenta.retirar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"No se puede retirar una cantidad negativa"));
		}
	}// test3

	@Test
	public void test4() {
		try {
			cuenta.retirar(500);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals("Saldo insuficiente"));
		}
	}// test4
}// EscenarioATest
