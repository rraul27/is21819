package es.unican.is2.refactorizaciones;

public class Administrativo {

	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String dni;
	private Nomina nomina;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getPrimerApellido() {
		return primerApellido;
	}
	
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	
	public String getSegundoApellido() {
		return segundoApellido;
	}
	
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public Nomina getNomina() {
		return nomina;
	}
	
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	
	public InformacionFiscal generarInformacionFiscal() {
		
		InformacionFiscal result = new InformacionFiscal();
		return result;
		
	} // InformacionFiscal
	
} // Administrativo
