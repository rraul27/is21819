package es.unican.is2.refactorizaciones;

import java.util.Calendar;
import java.util.Set;
import java.util.HashSet;

public class Taller {
	
	Set<Mecanico> mecanicos = new HashSet<Mecanico>();
	private String name = "Benito and Manolo";
	
	public void imprimirListaPersonal() {
		
		System.out.println(" Lista de Trabajadores Taller " + this.name );
		System.out.println("=======================================================================");
		
		for(Mecanico m : mecanicos) {
			System.out.println(m.getPrimerApellido() + " " + m.getSegundoApellido() + ", " + m.getNombre());
		} // for
		
	} // imprimirListaPersonal

	public void imprimirFacturas(Calendar fin, Calendar end) {
		// Do nothing
	} //
	
	public double calcularIva(Factura f) {
		
		return f.getValue()*0.21;
				
	} // calcularIva
	
} // Taller
