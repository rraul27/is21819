package es.unican.us2.Lavavajillas;

public class ControladorLuz {


	private Boolean state;


	public ControladorLuz() {
		state = false;
	}


	public void on() {
		state = true;
	}

	public void off() {
		state = false;
	}
	
	public Boolean getState() {
		return state;
	}
}
