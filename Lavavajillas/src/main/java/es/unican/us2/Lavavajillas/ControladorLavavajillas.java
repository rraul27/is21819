package es.unican.us2.Lavavajillas;

public abstract class ControladorLavavajillas {


	public static Apagado estadoApagado = new Apagado();
	public static Encendido estadoEncendido = new Encendido();
	public static Pausa estadoPausa = new Pausa();
	public static Lavando estadoLavando = new Lavando();
	
	
	public static ControladorLavavajillas init(Lavavajillas context) {
		estadoApagado.entryAction(context);
		return estadoApagado;
	}


	public void entryAction (Lavavajillas context) {
		
	}
	
	
	public void doAction(Lavavajillas context) {
		
		
		
		
	}
	
	
	public void exitAction(Lavavajillas context) {
		
		
		
		
	}
		
	
	public void programaSeleccionado(Lavavajillas context, ProgramaLavado programa ) {

	}


	public void onOff(Lavavajillas context) {

		

	}


	public void puertaAbierta(Lavavajillas context) {



	}


	public void puertaCerrada(Lavavajillas context) {



	}


	public void arrancar(Lavavajillas context) {





	}






}
