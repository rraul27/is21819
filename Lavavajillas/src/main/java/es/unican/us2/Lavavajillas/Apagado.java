package es.unican.us2.Lavavajillas;

public class Apagado extends ControladorLavavajillas{




	public void entryAction(Lavavajillas context) {
		context.setArrancar(false);
		context.setTiempoFinLavado(0);
		//LUCES OFF

	}

	public void onOff(Lavavajillas context) {
		this.exitAction(context);
		context.setState(estadoEncendido);
		estadoEncendido.entryAction(context);
		estadoEncendido.doAction(context);		
	}	

}
