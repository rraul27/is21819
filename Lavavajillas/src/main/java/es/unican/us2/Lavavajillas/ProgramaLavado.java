package es.unican.us2.Lavavajillas;


public class ProgramaLavado {

	
		private String nombre;
		private int tiempo;
		
		
		public ProgramaLavado(String nombre, int tiempo) {
			this.tiempo = tiempo;
			this.nombre = nombre;		
			
		}
		
		public String getNombre() {
			return nombre;
		}
		public int getTiempo() {
			return tiempo;
		}
		
}
