package es.unican.us2.Lavavajillas;



import java.util.HashMap;

public class Lavavajillas {

	private boolean arrancar;
	private int tiempoFinLavado;
	private HashMap<String, ControladorLuz> lucesProgramas;
	private HashMap<String, ProgramaLavado> programas;
	private ProgramaLavado programaElegido;
	private ControladorLavavajillas state;


	public void enciendeLuz() {
		if(programaElegido != null) {
			
			
			
			
			
		}
	}
	
	public boolean getArrancar() {
		return arrancar;
	}

	public void setArrancar(boolean arrancar) {
		this.arrancar = arrancar;
	}

	public int getTiempoFinLavado() {
		return tiempoFinLavado;
	}

	public void setTiempoFinLavado(int tiempoFinLavado) {
		this.tiempoFinLavado = tiempoFinLavado;
	}

	public HashMap<String, ControladorLuz> getLucesProgramas() {
		return lucesProgramas;
	}

	public void setLucesProgramas(HashMap<String, ControladorLuz> lucesProgramas) {
		this.lucesProgramas = lucesProgramas;
	}

	public HashMap<String, ProgramaLavado> getProgramas() {
		return programas;
	}

	public void setProgramas(HashMap<String, ProgramaLavado> programas) {
		this.programas = programas;
	}

	public ProgramaLavado getProgramaElegido() {
		return programaElegido;
	}

	public void setProgramaElegido(ProgramaLavado programaElegido) {
		this.programaElegido = programaElegido;
	}

	public ControladorLavavajillas getState() {
		return state;
	}

	public Lavavajillas() {
		programas = new HashMap<String, ProgramaLavado>();
		programas.put("Eco",new ProgramaLavado("Eco", 14));
		programas.put("Fast",new ProgramaLavado("Fast", 5));
		programas.put("Vidrio",new ProgramaLavado("Vidrio", 25));
		programas.put("Intenso",new ProgramaLavado("Intenso", 20));

		lucesProgramas = new HashMap<String, ControladorLuz>();
		lucesProgramas.put("Eco",new ControladorLuz());
		lucesProgramas.put("Fast",new ControladorLuz());
		lucesProgramas.put("Vidrio",new ControladorLuz());
		lucesProgramas.put("Intenso",new ControladorLuz());


		state = ControladorLavavajillas.init(this);

		arrancar = false;
		programaElegido = null;
		tiempoFinLavado = 0;
		
	}

	public void setState(ControladorLavavajillas value) {
		this.state = value;
	}


	public void start() {		
	}

	public void stop() {
	}

	public void programaSeleccionado( ProgramaLavado programa ) {
		this.programaElegido = programa;
		state.programaSeleccionado(this, programa);
	}

	public void onOff() {
		state.onOff(this);
	}

	public void puertaAbierta() {
		state.puertaAbierta(this);
	}

	public void puertaCerrada() {
		state.puertaCerrada(this);
	}

	public void arrancar() {
		state.arrancar(this);
	}

}
