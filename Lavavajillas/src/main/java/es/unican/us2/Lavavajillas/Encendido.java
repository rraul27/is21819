package es.unican.us2.Lavavajillas;

public class Encendido extends ControladorLavavajillas{





	public void entryAction (Lavavajillas context) {
		context.setState(this);
		context.enciendeLuz();
	}


	public void onOff(Lavavajillas context) {
		this.exitAction(context);
		context.setState(estadoApagado);
		estadoApagado.entryAction(context);
		estadoApagado.doAction(context);	

	}



	public void arrancar(Lavavajillas context) {
		this.exitAction(context);
		context.setArrancar(true);
		this.entryAction(context);
	}


	public void puertaCerrada(Lavavajillas context) {
		if(context.getArrancar() == true) {	
			this.exitAction(context);
			context.setState(estadoLavando);
			estadoLavando.entryAction(context);
			estadoLavando.doAction(context);
		}
	}

	public void programaSeleccionado(Lavavajillas context, ProgramaLavado programa ) {
		this.exitAction(context);
		context.programaSeleccionado(programa);
		this.entryAction(context);

	}



}
