package es.unican.is2.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import es.unican.is2.Lavavajillas.Controlador;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class I_Lavavajillas {

	private JFrame frmLavavajillas;
	private I_Lavavajillas window = this;;
	private JLabel puerta_abierta;
	private JLabel puerta_cerrada;
	private JLabel lblAbierta;
	private JLabel lblCerrada;
	private JLabel lblApagado;	
	private JLabel lblEncendido;
	private JLabel lblPausa;
	private JLabel lblLavando;
	private boolean arranqueB;
	private boolean lavando;
	private boolean cerrada = false;
	private boolean onoff = false;
	private JButton arranque;
	private JButton eco;
	private JButton intenso;
	private JButton prelavado;
	private JButton rapido;
	private JButton ONOFF;

	private JLabel eco_grey;
	private JLabel intenso_grey;
	private JLabel rapido_grey;
	private JLabel prelavado_grey;
	private JLabel lblArranque_red;
	private JLabel lblArranque_green;
	private JLabel lblArranque_grey;
	private JLabel lblONOFFGrey;
	private JLabel lblONOFFGreen;


	private Controlador controlador;
	public I_Lavavajillas(Controlador c) {
		controlador=c;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frmLavavajillas = new JFrame();	
		frmLavavajillas.setTitle("LAVAVAJILLAS PADARA");
		frmLavavajillas.setForeground(Color.BLACK);
		frmLavavajillas.setBackground(Color.BLACK);
		frmLavavajillas.setIconImage(Toolkit.getDefaultToolkit().getImage(I_Lavavajillas.class.getResource("/media/logoPadaraSolutions.png")));
		frmLavavajillas.setBounds(100, 100, 720, 513);
		frmLavavajillas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel_lavavajillas = new JPanel();
		panel_lavavajillas.setBackground(Color.DARK_GRAY);
		frmLavavajillas.getContentPane().add(panel_lavavajillas, BorderLayout.CENTER);
		panel_lavavajillas.setLayout(null);

		JButton eco = new JButton("ECO [15s]");
		eco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		eco.setBounds(134, 54, 176, 46);
		panel_lavavajillas.add(eco);

		JButton rapido = new JButton("RAPIDO [5s]");
		rapido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		rapido.setBounds(134, 146, 176, 46);
		panel_lavavajillas.add(rapido);

		JButton intenso = new JButton("INTENSO [30s]");
		intenso.setBounds(134, 229, 176, 46);
		panel_lavavajillas.add(intenso);

		JButton prelavado = new JButton("PRELAVADO [20s]");
		prelavado.setBounds(134, 318, 176, 46);
		panel_lavavajillas.add(prelavado);

		final JLabel eco_grey = new JLabel("");
		eco_grey.setVerticalAlignment(SwingConstants.TOP);
		eco_grey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		eco_grey.setBounds(65, 62, 33, 33);
		panel_lavavajillas.add(eco_grey);


		final JLabel rapido_grey = new JLabel("");
		rapido_grey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		rapido_grey.setVerticalAlignment(SwingConstants.TOP);
		rapido_grey.setBounds(65, 152, 33, 33);
		panel_lavavajillas.add(rapido_grey);

		final JLabel intenso_grey = new JLabel("");
		intenso_grey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		intenso_grey.setVerticalAlignment(SwingConstants.TOP);
		intenso_grey.setBounds(65, 236, 33, 33);
		panel_lavavajillas.add(intenso_grey);

		final JLabel prelavado_grey = new JLabel("");
		prelavado_grey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		prelavado_grey.setVerticalAlignment(SwingConstants.TOP);
		prelavado_grey.setBounds(65, 324, 33, 33);
		panel_lavavajillas.add(prelavado_grey);

		final JLabel eco_green = new JLabel("");
		eco_green.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		eco_green.setVerticalAlignment(SwingConstants.TOP);
		eco_green.setBounds(65, 62, 33, 33);
		panel_lavavajillas.add(eco_green);
		eco_green.setVisible(true);
		JLabel rapido_green = new JLabel("");
		rapido_green.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		rapido_green.setVerticalAlignment(SwingConstants.TOP);
		rapido_green.setBounds(65, 152, 33, 33);
		panel_lavavajillas.add(rapido_green);

		JLabel intenso_green = new JLabel("");
		intenso_green.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		intenso_green.setVerticalAlignment(SwingConstants.TOP);
		intenso_green.setBounds(65, 236, 33, 33);
		panel_lavavajillas.add(intenso_green);

		JLabel prelavado_green = new JLabel("");
		prelavado_green.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		prelavado_green.setVerticalAlignment(SwingConstants.TOP);
		prelavado_green.setBounds(65, 324, 33, 33);
		panel_lavavajillas.add(prelavado_green);

		final JLabel lblArranque_green = new JLabel("");
		lblArranque_green.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		lblArranque_green.setBounds(387, 289, 33, 33);
		panel_lavavajillas.add(lblArranque_green);

		final JLabel lblArranque_red = new JLabel("");
		lblArranque_red.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/red_led.png")));
		lblArranque_red.setBounds(387, 289, 33, 33);
		panel_lavavajillas.add(lblArranque_red);

		lblApagado = new JLabel("Apagado");
		lblApagado.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblApagado.setForeground(Color.LIGHT_GRAY);
		lblApagado.setBackground(Color.LIGHT_GRAY);
		lblApagado.setBounds(517, 190, 120, 30);
		panel_lavavajillas.add(lblApagado);
		lblApagado.setVisible(true);

		lblEncendido = new JLabel("Encendido");
		lblEncendido.setHorizontalAlignment(SwingConstants.CENTER);
		lblEncendido.setForeground(Color.LIGHT_GRAY);
		lblEncendido.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEncendido.setBackground(Color.LIGHT_GRAY);
		lblEncendido.setBounds(432, 177, 251, 56);
		panel_lavavajillas.add(lblEncendido);
		lblEncendido.setVisible(false);

		lblLavando = new JLabel("Lavando");
		lblLavando.setForeground(Color.LIGHT_GRAY);
		lblLavando.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblLavando.setBackground(Color.LIGHT_GRAY);
		lblLavando.setBounds(517, 190, 89, 30);
		panel_lavavajillas.add(lblLavando);
		lblLavando.setVisible(false);

		lblPausa = new JLabel("Pausa");
		lblPausa.setHorizontalAlignment(SwingConstants.CENTER);
		lblPausa.setForeground(Color.LIGHT_GRAY);
		lblPausa.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPausa.setBackground(Color.LIGHT_GRAY);
		lblPausa.setBounds(496, 177, 111, 56);
		panel_lavavajillas.add(lblPausa);



		JButton arranque = new JButton("ARRANQUE");
		arranque.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {	


				if(cerrada == false && !lavando && onoff == true) {
					controlador.arranca();
					//setArranque(false);
					if(!controlador.arrancado()) {
						lblArranque_green.setVisible(false);
						lblArranque_red.setVisible(true);
						if(controlador.getPuertaAbierta()) {

						}else {

						}

					}else {
						//setArranque(true);
						lblArranque_green.setVisible(true);
						lblArranque_red.setVisible(false);
						if(controlador.getPuertaAbierta()) {

						}else {

						}

					}

					if(controlador.arranca()==true) {
						lblLavando.setVisible(true); 
					}
				}
			}
		});
		arranque.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		this.arranque = arranque;
		eco.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {	
				if(getArranque() == true && onoff == true) {
					eco_grey.setVisible(false);
					rapido_grey.setVisible(true);
					intenso_grey.setVisible(true);
					prelavado_grey.setVisible(true);
					controlador.programaSeleccionado("Eco");
				}
			}
		});

		rapido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(getArranque() == true && onoff == true) {
					eco_grey.setVisible(true);
					rapido_grey.setVisible(false);
					intenso_grey.setVisible(true);
					prelavado_grey.setVisible(true);
					controlador.programaSeleccionado("Rapido");
				}
			}
		});
		intenso.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(getArranque() == true && onoff == true) {
					eco_grey.setVisible(true);
					rapido_grey.setVisible(true);
					intenso_grey.setVisible(false);
					prelavado_grey.setVisible(true);	
					controlador.programaSeleccionado("Intenso");
				}
			}
		});
		prelavado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(getArranque() == true && onoff == true) {
					eco_grey.setVisible(true);
					rapido_grey.setVisible(true);
					intenso_grey.setVisible(true);
					prelavado_grey.setVisible(false);	
					controlador.programaSeleccionado("Prelavado");
				}
			}
		});

		this.prelavado = prelavado;
		this.eco = eco;
		this.intenso = intenso;
		this.rapido = rapido;

		arranque.setBounds(441, 244, 227, 120);
		panel_lavavajillas.add(arranque);

		puerta_abierta = new JLabel("");
		puerta_abierta.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/puerta_abierta.png")));
		puerta_abierta.setBounds(418, 70, 89, 89);
		panel_lavavajillas.add(puerta_abierta);

		JButton btnEstadoPuerta = new JButton("ESTADO PUERTA");
		btnEstadoPuerta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				I_EstadoPuerta es = new I_EstadoPuerta(window);
			}
		});
		btnEstadoPuerta.setBounds(517, 85, 151, 33);
		panel_lavavajillas.add(btnEstadoPuerta);

		puerta_cerrada = new JLabel("");
		puerta_cerrada.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/puerta_cerrada.png")));
		puerta_cerrada.setBounds(418, 70, 89, 89);
		panel_lavavajillas.add(puerta_cerrada);

		lblCerrada = new JLabel("CERRADA");
		lblCerrada.setForeground(Color.GREEN);
		lblCerrada.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCerrada.setBounds(561, 121, 89, 30);
		panel_lavavajillas.add(lblCerrada);

		final JLabel lblONOFFGrey = new JLabel("");
		lblONOFFGrey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		lblONOFFGrey.setVerticalAlignment(SwingConstants.BOTTOM);
		lblONOFFGrey.setBounds(234, 417, 33, 33);
		panel_lavavajillas.add(lblONOFFGrey);


		final JLabel lblArranque_grey = new JLabel("");
		lblArranque_grey.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/grey_led.png")));
		lblArranque_grey.setBounds(387, 289, 33, 33);
		panel_lavavajillas.add(lblArranque_grey);

		final JLabel lblONOFFGreen = new JLabel("");
		lblONOFFGreen.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/green_led.png")));
		lblONOFFGreen.setVerticalAlignment(SwingConstants.BOTTOM);
		lblONOFFGreen.setBounds(234, 417, 33, 33);
		panel_lavavajillas.add(lblONOFFGreen);

		JButton btnOnoff = new JButton("ON/OFF");
		btnOnoff.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!cerrada && !lavando) {			
					controlador.encenderApagar();
					if(controlador.getOnOff()){
						lblONOFFGrey.setVisible(false);
						lblONOFFGreen.setVisible(true);
						eco_green.setVisible(true);
						eco_grey.setVisible(false);
						lblArranque_grey.setVisible(false);
						lblArranque_red.setVisible(true);
						lblEncendido.setVisible(true);
						lblApagado.setVisible(false);
						setArranque(true);
						onoff = true;

					}else {
						eco_grey.setVisible(true);
						intenso_grey.setVisible(true);
						rapido_grey.setVisible(true);
						prelavado_grey.setVisible(true);
						lblArranque_red.setVisible(false);
						lblArranque_green.setVisible(false);
						lblArranque_grey.setVisible(true);
						lblONOFFGrey.setVisible(true);
						lblONOFFGreen.setVisible(false);
						lblEncendido.setVisible(false);
						lblApagado.setVisible(true);
						setArranque(false);
						onoff = false;
					}
				}
			}
		});
		btnOnoff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnOnoff.setBounds(281, 417, 151, 33);
		panel_lavavajillas.add(btnOnoff);

		this.ONOFF=btnOnoff;
		lblAbierta = new JLabel("ABIERTA");
		lblAbierta.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAbierta.setForeground(Color.RED);
		lblAbierta.setBounds(563, 121, 61, 30);
		panel_lavavajillas.add(lblAbierta);
		lblAbierta.setVisible(true);

		lblArranque_grey.setVisible(true);
		lblONOFFGreen.setVisible(false);
		lblONOFFGrey.setVisible(true);
		lblPausa.setVisible(false);
		lblCerrada.setVisible(false);
		puerta_cerrada.setVisible(false);
		puerta_abierta.setVisible(true);
		eco_grey.setVisible(true);
		lblArranque_red.setVisible(false);
		lblArranque_green.setVisible(false);
		frmLavavajillas.setLocationRelativeTo(null);
		frmLavavajillas.setVisible(true);	



		this.eco_grey = eco_grey;
		this.intenso_grey = intenso_grey;
		this.rapido_grey = rapido_grey;
		this.prelavado_grey = prelavado_grey;
		this.lblArranque_red = lblArranque_red;
		this.lblArranque_green = lblArranque_green;
		this.lblArranque_grey = lblArranque_grey;
		this.lblONOFFGrey = lblONOFFGrey;
		this.lblONOFFGreen = lblONOFFGreen;

		JLabel label = new JLabel("");
		label.setBounds(330, 462, 66, 15);
		panel_lavavajillas.add(label);

		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(I_Lavavajillas.class.getResource("/media/logoPadaraSolutions2.png")));
		label_1.setBounds(633, 388, 75, 85);
		panel_lavavajillas.add(label_1);

	}

	public JLabel getLblApagado() {
		return lblApagado;
	}
	public JLabel getPuerta_green() {
		return puerta_abierta;
	}

	public JLabel getPuerta_red() {
		return puerta_cerrada;
	}

	public JLabel getLblAbierta() {
		return lblAbierta;
	}

	public JLabel getLblCerrada() {
		return lblCerrada;
	}

	public Controlador getControlador() {
		return controlador;
	}

	public JLabel getLblPausa() {
		return lblPausa;
	}

	public Boolean getArranque() {
		return arranqueB;
	}
	public void setArranque(Boolean b) {
		this.arranqueB = b;
	}

	public JButton arranque() {
		return arranque;
	}

	public JLabel getLblLavando() {
		return lblLavando;
	}

	public JLabel getLblEncendido() {
		return lblEncendido;
	}

	public void cerrada(boolean b) {
		cerrada = (b);
	}


	public void bloquea(boolean b) {
		arranque.setEnabled(b); 
		eco.setEnabled(b);
		rapido.setEnabled(b);
		intenso.setEnabled(b);
		prelavado.setEnabled(b);
		ONOFF.setEnabled(b);
		cerrada(b);

	}

	public boolean lavando() {
		return lavando;
	}

	public void setLavando(boolean b) {
		this.lavando = b;
	}

	public void setTimer() {
		TimerLavando t = new TimerLavando();
		t.setPriority(Thread.MAX_PRIORITY);
		t.start();
	}


	public class TimerLavando extends Thread{

		@Override
		public void run() {

			while(lavando) {
				System.out.println(controlador.getTiempoFinLavado());
				if(controlador.getTiempoFinLavado()<1 || controlador.getTiempoFinLavado() == controlador.getTiempoPrograma()) {
					lavando = false;

				}
			}
			try {

				sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			getLblLavando().setVisible(false);
			getLblPausa().setVisible(false);
			getLblEncendido().setVisible(false);
			getLblApagado().setVisible(true);

			eco_grey.setVisible(true);
			intenso_grey.setVisible(true);
			rapido_grey.setVisible(true);
			prelavado_grey.setVisible(true);
			lblArranque_red.setVisible(false);
			lblArranque_green.setVisible(false);
			lblArranque_grey.setVisible(true);
			lblONOFFGrey.setVisible(true);
			lblONOFFGreen.setVisible(false);
			lblEncendido.setVisible(false);
			lblApagado.setVisible(true);
			setArranque(false);


		}
	}
}