package es.unican.is2.gui;

import es.unican.is2.Lavavajillas.Controlador;
import es.unican.is2.Lavavajillas.Lavavajillas;

public class Main {
	public static void main(String args[]) {
		Lavavajillas lavavajillas = new Lavavajillas();
		Controlador c=new Controlador(lavavajillas);
		I_Lavavajillas interfaz = new I_Lavavajillas(c);
	}
}
