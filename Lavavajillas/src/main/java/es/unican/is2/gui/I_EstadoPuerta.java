package es.unican.is2.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class I_EstadoPuerta {

	private JPanel contentPane;
	private JFrame frmI_EstadoPuerta;
	private I_Lavavajillas lavavajillas;
	/**
	 * Create the application.
	 */
	public I_EstadoPuerta(I_Lavavajillas le) {
		super();
		lavavajillas=le;
		initialize();	
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmI_EstadoPuerta = new JFrame();
		frmI_EstadoPuerta.setResizable(false);
		frmI_EstadoPuerta.setBackground(Color.DARK_GRAY);
		frmI_EstadoPuerta.setForeground(Color.DARK_GRAY);
		frmI_EstadoPuerta.setTitle("ESTADO PUERTA");
		frmI_EstadoPuerta.setIconImage(Toolkit.getDefaultToolkit().getImage(I_EstadoPuerta.class.getResource("/media/logoPadaraSolutions.png")));
		frmI_EstadoPuerta.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmI_EstadoPuerta.setBounds(100, 100, 551, 226);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frmI_EstadoPuerta.setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton PuertaAbierta = new JButton("PUERTA ABIERTA");
		PuertaAbierta.setBounds(38, 128, 214, 42);
		contentPane.add(PuertaAbierta);

		JButton PuertaCerrada = new JButton("PUERTA CERRADA");
		PuertaCerrada.setBounds(290, 128, 214, 42);
		contentPane.add(PuertaCerrada);

		final JLabel puerta_abierta = new JLabel("");
		puerta_abierta.setIcon(new ImageIcon(I_EstadoPuerta.class.getResource("/media/puerta_abierta.png")));
		puerta_abierta.setBounds(96, 28, 89, 89);
		contentPane.add(puerta_abierta);

		final JLabel puerta_cerrada = new JLabel("");
		puerta_cerrada.setIcon(new ImageIcon(I_EstadoPuerta.class.getResource("/media/puerta_cerrada.png")));
		puerta_cerrada.setBounds(351, 28, 89, 95);		
		contentPane.add(puerta_cerrada);
		puerta_cerrada.setVisible(false);	
		if(lavavajillas.getControlador().getPuertaAbierta()) {
			puerta_cerrada.setVisible(false);	
			puerta_abierta.setVisible(true);
		}else {
			puerta_cerrada.setVisible(true);	
			puerta_abierta.setVisible(false);
		}

		PuertaAbierta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				puerta_cerrada.setVisible(false);	
				puerta_abierta.setVisible(true);		
				lavavajillas.bloquea(true);
				lavavajillas.cerrada(false);
				lavavajillas.getControlador().puertaAbierta();
				lavavajillas.getPuerta_red().setVisible(false);
				lavavajillas.getPuerta_green().setVisible(true);
				lavavajillas.getLblAbierta().setVisible(true);
				lavavajillas.getLblCerrada().setVisible(false);
				if(lavavajillas.lavando()) {
					lavavajillas.getLblLavando().setVisible(false);
					lavavajillas.getLblPausa().setVisible(true);
				}else {
					lavavajillas.getLblLavando().setVisible(false);
					lavavajillas.setArranque(true);
					lavavajillas.getLblPausa().setVisible(false);
				}				
			}
		});
		PuertaCerrada.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				puerta_cerrada.setVisible(true);	
				puerta_abierta.setVisible(false);	
				lavavajillas.bloquea(false);
				lavavajillas.cerrada(true);
				lavavajillas.getLblAbierta().setVisible(false);
				lavavajillas.getLblCerrada().setVisible(true);
				lavavajillas.getControlador().puertaCerrada();
				lavavajillas.getPuerta_red().setVisible(true);
				lavavajillas.getPuerta_green().setVisible(false);
				if(lavavajillas.getControlador().getArrancado()) {
					lavavajillas.getLblEncendido().setVisible(false);
					lavavajillas.getLblLavando().setVisible(true);
					lavavajillas.getLblPausa().setVisible(false);
					lavavajillas.setLavando(true);
					lavavajillas.setArranque(false);
					lavavajillas.setTimer();
				}
				if(lavavajillas.lavando()) {
					lavavajillas.getLblEncendido().setVisible(false);
					lavavajillas.getLblLavando().setVisible(true);
					lavavajillas.setArranque(false);
					lavavajillas.getLblPausa().setVisible(false);
				}
			}
		});
		frmI_EstadoPuerta.setLocationRelativeTo(null);
		this.frmI_EstadoPuerta.setVisible(true);
	}
}
