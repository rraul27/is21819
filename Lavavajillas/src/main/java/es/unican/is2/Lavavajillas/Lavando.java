package es.unican.is2.Lavavajillas;

import java.util.*;


public class Lavando extends ControladorLavavajillas {
	private int timeLeft;
	private boolean empezado = false;

	Lavavajillas contexto = null;
	Timer timer;


	public class TimerLavando extends TimerTask{

		@Override
		public void run() {
			contexto.restaTiempoFinLavado();
			System.out.print(contexto.getTiempoFinLavado());
			
			if(contexto.getTiempoFinLavado() == 0) {
				empezado = false;
				contexto.setState(getEstadoApagado());
				getEstadoApagado().entryAction(contexto);
				getEstadoApagado().doAction(contexto);
				exitAction(contexto);
				timer.cancel();
			}
		}
	}

	public Lavando() {
		super();
	}
	TimerLavando t;

	@Override
	public void entryAction(Lavavajillas context) {
		contexto = context;	

		t = new TimerLavando();
		timer = new Timer(true);

		if(empezado == true) {			
			context.setTime(timeLeft);
		}

		timer.scheduleAtFixedRate(t, 0, 1000);
		context.start();
	}


	@Override
	public void exitAction(Lavavajillas context) {
		context.stop();
		context.setState(getEstadoApagado());
		getEstadoApagado().entryAction(context);
		getEstadoApagado().doAction(context);
	}

	@Override
	public void PuertaAbierta(Lavavajillas context)
	{
		timer.cancel();
		timeLeft = contexto.getTiempoFinLavado();
		empezado = true;
		context.setPuertaAbierta(true);
		contexto.setState(getEstadoPausa());
		getEstadoPausa().entryAction(contexto);
		getEstadoPausa().doAction(contexto);
	}


}
