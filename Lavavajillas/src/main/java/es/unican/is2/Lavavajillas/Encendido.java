package es.unican.is2.Lavavajillas;

public class Encendido extends ControladorLavavajillas {
	public Encendido() {
		super();
	}


	@Override
	public void OnOff(Lavavajillas context)
	{
		context.pressOnOff();
		context.setState(getEstadoApagado());
		getEstadoApagado().entryAction(context);
	}


	@Override
	public void PuertaCerrada(Lavavajillas context)
	{			
		context.setPuertaAbierta(false);
		if(context.getArrancar()==true) {
			this.exitAction(context);
			context.setState(getEstadoLavando());
			getEstadoLavando().entryAction(context);
			getEstadoLavando().doAction(context);
		}
	}

	@Override
	public void PuertaAbierta(Lavavajillas context)
	{			
		context.setPuertaAbierta(true);
	}



	@Override
	public void Arrancar(Lavavajillas context)
	{
		if(context.getPuertaAbierta() != false){
			context.setArrancar(!context.getArrancar());

			if(context.getPuertaAbierta()==false && context.getArrancar()) {
				this.exitAction(context);		
				context.setState(getEstadoLavando());
				getEstadoLavando().entryAction(context);
			}
		}
	}
} 


