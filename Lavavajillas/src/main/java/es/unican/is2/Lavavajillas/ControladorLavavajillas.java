package es.unican.is2.Lavavajillas;

public abstract class ControladorLavavajillas {
	private static Apagado estadoApagado = new Apagado();
	private static Encendido estadoEncendido = new Encendido();
	private static Pausa estadoPausa = new Pausa();
	private static Lavando estadoLavando = new Lavando();

	
	
	
	public static Apagado getEstadoApagado() {
		return estadoApagado;
	}

	public static Encendido getEstadoEncendido() {
		return estadoEncendido;
	}

	public static Pausa getEstadoPausa() {
		return estadoPausa;
	}

	public static Lavando getEstadoLavando() {
		return estadoLavando;
	}
	

	public static ControladorLavavajillas init(Lavavajillas context) {
		estadoApagado.entryAction(context);
		return estadoApagado;
	}

	public void entryAction(Lavavajillas context) {}

	public void doAction(Lavavajillas context) {}
	
	public void exitAction(Lavavajillas context) {}


	
	public void OnOff(Lavavajillas context) {}

	public void PuertaCerrada(Lavavajillas context){}

	public void PuertaAbierta(Lavavajillas context){}

	public void Arrancar(Lavavajillas context){}

}
