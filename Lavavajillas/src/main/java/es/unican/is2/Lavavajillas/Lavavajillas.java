package es.unican.is2.Lavavajillas;

import java.util.HashMap;



/**
 * Lavavajillas
 *
 */
public class Lavavajillas 
{


	private boolean ONOFF = false;
	private boolean arrancado;
	private boolean puertaAbierta;
	private int tiempoFinLavado;
	private ControladorLavavajillas state;
	private HashMap<String, ProgramaLavado> programas;
	private HashMap<String, Luz> luces;
	private Luz luzEncendida;
	private ProgramaLavado programaElegido;


	public Lavavajillas()
	{
		programas = new HashMap<String, ProgramaLavado>();
		programas.put("Eco", new ProgramaLavado("Eco",15));
		programas.put("Rapido", new ProgramaLavado("Rapido",5));
		programas.put("Intenso", new ProgramaLavado("Intenso",30));
		programas.put("Prelavado", new ProgramaLavado("Prelavado",20));
		luces = new HashMap<String, Luz>();
		luces.put("Eco", new Luz());
		luces.put("Rapido", new Luz());
		luces.put("Intenso", new Luz());
		luces.put("Prelavado", new Luz());
	
		programaElegido = programas.get("Eco");
		luzEncendida = luces.get("Eco");
		arrancado=false;
		puertaAbierta=true;
		tiempoFinLavado=15;
		state = ControladorLavavajillas.init(this);	

	}

	public void setState(ControladorLavavajillas c) {
		state=c;		
	}

	public void start()
	{
		//System.out.println("Empieza a lavar");
	}
	public void stop()
	{
		//System.out.println("Acaba a lavar");
		
	}

	public ProgramaLavado programaSeleccionado(ProgramaLavado p) {
		return p;
	}

	public void setProgramaElegido(ProgramaLavado programa) {
		programaElegido = programa;
		tiempoFinLavado = programa.getTiempo();
		apagaLuz();
		enciendeLuz(programa.getNombre());
	}
	public ProgramaLavado buscaPrograma(String nombre) {
		return programas.get(nombre);
	}
	public void OnOff()
	{
		state.OnOff(this);
	}
	public void PuertaCerrada()
	{
		state.PuertaCerrada(this);
	}
	public void PuertaAbierta()
	{
		state.PuertaAbierta(this);
	}
	public void Arrancar()
	{
		state.Arrancar(this);
	}

	public void setArrancar(boolean b) {
		arrancado=b;
	}

	public boolean getArrancar() {
		return arrancado;
	}

	public void pressOnOff() {
		if (tiempoFinLavado == 0) {
			ONOFF = false;
		}else {		
		if(ONOFF == true) ONOFF = false;
		else ONOFF = true;
		}
	}
	
	public boolean getOnOff() {
		return ONOFF;
	}
	
	public void setPuertaAbierta(boolean estado) {
		puertaAbierta = estado;
	}

	public boolean getPuertaAbierta() {
		return puertaAbierta;
	}

	public void restaTiempoFinLavado() {
		tiempoFinLavado --;
	}

	public int getTiempoFinLavado() {
		return tiempoFinLavado;
	}
	public void apagaLuz() {
		luzEncendida.off();
	}
		
	public void enciendeLuz(String nombre) {
		luces.get(nombre).on();
		luzEncendida = luces.get(nombre);
	}

	public ProgramaLavado getProgramaLavado() {
		return programaElegido;
	}

	public void setTime(int t) {
		tiempoFinLavado = t;
		
	}

	public void setOff() {
		ONOFF = false;
				
	}
	

}
