package es.unican.is2.Lavavajillas;

public class ProgramaLavado {
	private String nombre;
	private int tiempo;
	
	public ProgramaLavado(String string, int i) {
		nombre=string;
		tiempo=i;
	}

	public int getTiempo() {
		return tiempo;
	}

	public String getNombre() {
		return nombre;
	}

}
