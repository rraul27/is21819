package es.unican.is2.Lavavajillas;

public class Controlador {
	private Lavavajillas lavavajillas;	
	
	
	
	public Controlador(Lavavajillas l) {
		lavavajillas=l;
	}

	public boolean arranca() {
		lavavajillas.Arrancar();
		if(lavavajillas.getArrancar()&&!lavavajillas.getPuertaAbierta()) {
			return true;
		}
		return false;
	}
	public void programaSeleccionado(String nombre) {		
		lavavajillas.setProgramaElegido(lavavajillas.buscaPrograma(nombre));	
	}

	public void puertaAbierta() {		
		lavavajillas.PuertaAbierta();		
	}
	public void puertaCerrada() {		
		lavavajillas.PuertaCerrada();		
	}
	public boolean arrancado() {
		boolean estado = lavavajillas.getArrancar();
		lavavajillas.setArrancar(!estado);
		return estado;
	}
	public boolean getArrancado() {
		return lavavajillas.getArrancar();
	}
	public boolean getPuertaAbierta() {
		return lavavajillas.getPuertaAbierta();
	}

	public int getTiempoFinLavado() {
		return lavavajillas.getTiempoFinLavado();
	}
	
	public int getTiempoPrograma() {
		return lavavajillas.getProgramaLavado().getTiempo();
	}

	public boolean restaTiempoFinLavado() {	
		if(lavavajillas.getArrancar()&&!lavavajillas.getPuertaAbierta()) {
			lavavajillas.restaTiempoFinLavado();
			if(lavavajillas.getTiempoFinLavado() == 0) {
				return true;
			}
		}
		return false;
	}

	public void encenderApagar() {		
		lavavajillas.OnOff();
		
	}
	
	public boolean getOnOff() {
		return lavavajillas.getOnOff();
	}
}
