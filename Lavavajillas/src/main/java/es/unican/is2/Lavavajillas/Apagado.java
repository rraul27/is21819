package es.unican.is2.Lavavajillas;

public class Apagado extends ControladorLavavajillas {

	public Apagado() {
		super();
	}

	@Override
	public void entryAction(Lavavajillas context) {
		context.setArrancar(false);
		context.apagaLuz();
		context.setOff();
		context.setTime(15);
	}
	
	@Override
	public void OnOff(Lavavajillas context)
	{
		context.pressOnOff();
		context.setState(getEstadoEncendido());
		getEstadoEncendido().entryAction(context);
		getEstadoEncendido().doAction(context);
		
	}
	
	@Override
	public void PuertaAbierta(Lavavajillas context)
	{			
		context.setPuertaAbierta(true);
	}
	
	@Override
	public void PuertaCerrada(Lavavajillas context)
	{			
		context.setPuertaAbierta(false);
	}
}
