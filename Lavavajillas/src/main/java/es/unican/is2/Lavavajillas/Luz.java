package es.unican.is2.Lavavajillas;

public class Luz {
	
	private boolean encendido ;
	
	public Luz() {
		encendido = false;
	}
	public void on() {
		encendido=true;
	}
	public void off() {
		encendido=false;
	}
	
	public boolean getEncendido() {
		return encendido;
	}
	
}
