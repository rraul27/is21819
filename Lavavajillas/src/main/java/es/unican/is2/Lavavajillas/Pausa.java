package es.unican.is2.Lavavajillas;

public class Pausa extends ControladorLavavajillas {
	public Pausa() {
		super();
	}
	
	
	@Override
	public void PuertaCerrada(Lavavajillas context)
	{
		context.setPuertaAbierta(false);
		exitAction(context);
		context.setState(getEstadoLavando());
		getEstadoLavando().entryAction(context);
	}

}
