package es.unican.is2.practica4.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import es.unican.is2.practica4.CinesUC_19;
import es.unican.is2.practica4.DatoIncorrectoException;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;
import javax.swing.DropMode;

@SuppressWarnings("serial")
public class CinesUCGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtFechaUltimaVisita;	
	private JTextField txtPrecio;
	private final JLabel lblConsumo = new JLabel("Puntos");
	private JTextField txtPuntos;
	private JRadioButton btnVIP;
	private JTextField txtFecha;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CinesUCGUI frame = new CinesUCGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CinesUCGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 408, 213);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtFechaUltimaVisita = new JTextField();
		txtFechaUltimaVisita.setText("dd/mm/yyyy");
		txtFechaUltimaVisita.setName("txtFechaUltimaVisita");
		txtFechaUltimaVisita.setBounds(124, 8, 86, 20);
		contentPane.add(txtFechaUltimaVisita);
		txtFechaUltimaVisita.setColumns(10);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha Ultima Visita");
		lblFechaNacimiento.setBounds(10, 11, 114, 14);
		contentPane.add(lblFechaNacimiento);
		
		JLabel lblPrecioAbono = new JLabel("PRECIO");
		lblPrecioAbono.setBounds(10, 140, 126, 17);
		contentPane.add(lblPrecioAbono);
		
		txtPrecio = new JTextField();
		txtPrecio.setName("txtPrecio");
		txtPrecio.setBounds(100, 140, 208, 18);
		contentPane.add(txtPrecio);
		txtPrecio.setColumns(10);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.setName("btnCalcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			    LocalDate fechaUltimaVisita = LocalDate.parse(txtFechaUltimaVisita.getText(), formatter);
			    LocalDate fecha = LocalDate.parse(txtFecha.getText(), formatter);
			    int puntos = Integer.parseInt(txtPuntos.getText());
			    boolean vip = btnVIP.isSelected();
			    double precio;
				try {
					precio = CinesUC_19.precioEntradaSocio(fechaUltimaVisita, fecha, vip, puntos);

					txtPrecio.setText(Double.toString(precio));
				} catch (DatoIncorrectoException e) {
					txtPrecio.setText(e.getMensaje());
				}
			 
			}
		});
		btnCalcular.setBounds(72, 78, 126, 29);
		contentPane.add(btnCalcular);
		
		lblConsumo.setBounds(222, 36, 98, 31);
		contentPane.add(lblConsumo);
		
		txtPuntos = new JTextField();
		//txtPuntos.setDropMode(DropMode.ON);
		txtPuntos.setBounds(290, 41, 86, 20);
		contentPane.add(txtPuntos);
		txtPuntos.setColumns(10);
		txtPuntos.setName("txtPuntos");
		
		btnVIP = new JRadioButton("Butaca VIP");
		btnVIP.setBounds(277, 7, 109, 23);
		btnVIP.setName("btnVIP");
		contentPane.add(btnVIP);
		
		txtFecha = new JTextField();
		txtFecha.setText("dd/mm/yyyy");
		txtFecha.setName("txtFecha");
		txtFecha.setBounds(124, 39, 86, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(35, 44, 46, 14);
		contentPane.add(lblFecha);
		
	}
}