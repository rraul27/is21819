package es.unican.is2.practica4;

public class DatoIncorrectoException extends Exception {

	private String mensaje;
	
	public DatoIncorrectoException(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getMensaje() {
		return mensaje;
	}
}
