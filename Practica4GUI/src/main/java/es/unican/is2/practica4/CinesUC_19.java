package es.unican.is2.practica4;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class CinesUC_19 {

	public static double precioEntradaSocio(LocalDate fechaUltimaVisita, LocalDate fecha, boolean vip, int puntos) throws DatoIncorrectoException {
		//Comrpobamos validad de datos
		if(puntos < 0)     throw new DatoIncorrectoException("Puntos Incorrectos");        
		else if(fechaUltimaVisita.until(fecha).getDays() < 0)	throw new DatoIncorrectoException("Fecha Ultima Visita incorrecta");
		else if(fecha.compareTo(LocalDate.now()) <0) throw new DatoIncorrectoException("Fecha actual erronea");
		double precio;
		//Puntos
		if(puntos >= 1000) return 0;
		//Precio dia de la semana
		if(fecha.getDayOfWeek() == DayOfWeek.WEDNESDAY) precio = 5;
		else if(fecha.getDayOfWeek() == DayOfWeek.SATURDAY || fecha.getDayOfWeek() == DayOfWeek.SUNDAY)    precio = 8;
		else precio = 7;
		//Precio butaca vip
		if(vip) precio += 1.2;
		//Descuenta en menos de 15 dias
		if(fechaUltimaVisita.until(fecha).getDays() <15 && fechaUltimaVisita.until(fecha).getMonths() == 0) precio = precio - (precio * 0.25);

		
		return (Math.round(precio * 100.0) / 100.0);

	}
}