package es.unican.is2.practica4.listaOrdenada;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

//import es.unican.is2.containers.ListaOrdenada;
import es.unican.is2.practica4.listaOrdenadaBuena.*;


public class ListaOrdenadaTest {

	private ListaOrdenada<Integer> list;



	@Before
	public void setUp() {
		this.list = new ListaOrdenada<Integer>();
	}


	@Test
	public void listOrdenadaTestAddSize() {

		this.list = new ListaOrdenada<Integer>();

		// Casos de prueba válidos

		// Size lista vacia
		try {
			assertTrue(list.size() == 0);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		// Add y size, 1 elemento a lista vacia
		try {
			list.add(1);
			assertTrue(list.size() == 1);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		//Add y size, 1 elemento a lista con 1 elemento

		try {
			list.add(3);
			assertTrue(list.size() == 2);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}


		//Add y size, 1 elemento a lista con varios elemento
		try {
			list.add(2);list.add(4);list.add(5);
			assertTrue(list.size() == 5);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		//		 Casos de prueba inválidos
		Throwable caught = null;
		try {
			list.add(null);
		} catch (Throwable t) {
			caught = t;
		}
		assertNotNull(caught);

	}

	@Test
	public void listOrdenadaTestClear() {
		this.list = new ListaOrdenada<Integer>();
		// Casos de prueba válidos

		//Clear lista vacia
		try {
			list.clear();
			assertTrue(list.size() == 0);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}


		//Clear 1 elemento
		try {
			list.add(1);
			list.clear();
			assertTrue(list.size() == 0);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}



		//Clear varios elemento
		try {
			list.add(1);list.add(2);list.add(3);list.add(4);list.add(5);
			list.clear();
			assertTrue(list.size() == 0);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}
	}

	@Test
	public void listOrdenadaTestGet() {
		this.list = new ListaOrdenada<Integer>();
		// Casos de prueba válidos



		//Get 1 elemento
		try {
			list.add(1);
			assertTrue(list.get(0) == 1);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		//Get 1 elemento
		try {
			list.add(2);list.add(3);list.add(4);list.add(5);
			assertTrue(list.get(1) == 2);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		//Get 1 elemento
		try {
			assertTrue(list.get(4) == 5);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		// Casos de prueba inválidos
		Throwable caught = null;
		try {
			list.get(5);
		} catch (Throwable t) {
			caught = t;
		}
		assertNotNull(caught);


		try {
			list.get(10);
		} catch (Throwable t) {
			caught = t;
		}
		assertNotNull(caught);


	}


	@Test
	public void listOrdenadaTestRemove() {
		this.list = new ListaOrdenada<Integer>();
		// Casos de prueba válidos

		//Remove lista 1 elemento
		;
		try {
			list.add(1);
			int x = list.remove(0);
			assertTrue(list.size() == 0 && x == 1);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		//Remove lista varios elementos
		try {
			list.add(1);list.add(2);list.add(3);list.add(4);list.add(5);
			int x = list.remove(1);
			assertTrue(list.size() == 4 && x == 2);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		try {
			list.add(2);
			int x = list.remove(4);
			assertTrue(list.size() == 4 && x == 5);
		} catch (IndexOutOfBoundsException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}


		Throwable caught = null;
		try {
			list.remove(5);
		} catch (Throwable t) {
			caught = t;
		}
		assertNotNull(caught);


		try {
			list.remove(5);
		} catch (Throwable t) {
			caught = t;
		}
		assertNotNull(caught);


	}
}