package es.unican.is2.practica4.gui;

import static org.junit.Assert.assertTrue;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.unican.is2.practica4.CinesUC_19;

public class CinesUCGUITest {

	private FrameFixture demo;

	@Before
	public void setUp() {
		CinesUCGUI sut = new CinesUCGUI();
		demo = new FrameFixture(sut);
		sut.setVisible(true);
	}

	@After
	public void tearDown() {
		demo.cleanUp();
	}

	@Test
	public void test() {
		
		//Para escribir en el campo de texto de tipo fecha
		LocalDate now = LocalDate.now();
		//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		//String fechaString = now.format(formatter);
		
		
		
		// Comprobamos que la interfaz tiene el aspecto deseado
		demo.button("btnCalcular").requireText("CALCULAR");
		
		
		
		//Test 1
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.WEDNESDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.WEDNESDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("0");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("4.65");
		//
			
		//Test 2
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.SATURDAY).plusDays(7).minusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.SATURDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("450");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("6.0");
		//
	
		//Test 3
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.SUNDAY).plusDays(7).minusDays(14).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.SUNDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("999");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("6.9");
		//
		
		//Test 4
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.MONDAY).plusDays(7).minusDays(15).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.MONDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("1000");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("0.0");
		//
		
		//Test 5
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.TUESDAY).plusDays(7).minusDays(365).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.TUESDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("9999");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("0.0");
		//
		
		//Test 6
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.THURSDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.THURSDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("0");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("5.25");
		//
		
		//Test 7
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("450");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("6.15");
		//		
		
		
		
		//Test 1
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.WEDNESDAY).plusDays(7).plusDays(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.WEDNESDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		//demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("800");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("Fecha Ultima Visita incorrecta");
		//		
			
		//Test 2
		demo.textBox("txtFechaUltimaVisita").setText(now.minusDays(14).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.minusDays(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("999");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("Fecha actual erronea");
		//	
		
		//Test 3
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.SUNDAY).plusDays(7).minusDays(15).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.SUNDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("-1");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("Puntos Incorrectos");
		//		
				
		
		
		//Test 1
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(25).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("0");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("7.0");
		//		
		
		//Test 2
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(50).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		//demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("0");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("7.0");
		//		
		
		//Test 3
		demo.textBox("txtFechaUltimaVisita").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(31).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		demo.textBox("txtFecha").setText(now.with(DayOfWeek.FRIDAY).plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		//demo.radioButton("btnVIP").click();
		demo.textBox("txtPuntos").setText("0");
			
		demo.button("btnCalcular").click();
		
		demo.textBox("txtPrecio").requireText("7.0");
		//		
		
		
		
		// Sleep para visualizar como se realiza el test
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}