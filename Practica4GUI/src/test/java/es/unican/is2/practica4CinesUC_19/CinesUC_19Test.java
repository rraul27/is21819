package es.unican.is2.practica4CinesUC_19;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.DayOfWeek;
import java.time.LocalDate;

import org.junit.*;

import es.unican.is2.practica4.CinesUC_19;
import es.unican.is2.practica4.DatoIncorrectoException;



public class CinesUC_19Test {

	private CinesUC_19 cine;
	private LocalDate now = LocalDate.now();
	private Double precio;


	@Before
	public void setUp() {
		this.cine = new CinesUC_19();
	}

	@Test
	public void precioEntradaSocioTest() {

		// Casos de prueba válidos		

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.WEDNESDAY).plusDays(7),now.with(DayOfWeek.WEDNESDAY).plusDays(7), true, 0);
			assertTrue(precio.toString(), precio == 4.65);
		} catch (DatoIncorrectoException e) {
			fail("Caso 1: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.SATURDAY).plusDays(7).minusDays(7),now.with(DayOfWeek.SATURDAY).plusDays(7), false, 450);
			assertTrue(precio.toString(), precio == 6);
		} catch (DatoIncorrectoException e) {
			fail("Caso 2: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.SUNDAY).plusDays(7).minusDays(14),now.with(DayOfWeek.SUNDAY).plusDays(7), true, 999);
			assertTrue(precio.toString(), precio == 6.90);
		} catch (DatoIncorrectoException e) {
			fail("Caso 3: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.MONDAY).plusDays(7).minusDays(15),now.with(DayOfWeek.MONDAY).plusDays(7), false, 1000);
			assertTrue(precio.toString(), precio == 0);
		} catch (DatoIncorrectoException e) {
			fail("Caso 4: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.TUESDAY).plusDays(7).minusDays(365),now.with(DayOfWeek.TUESDAY).plusDays(7), true, 9999);
			assertTrue(precio.toString(), precio ==  0);
		} catch (DatoIncorrectoException e) {
			fail("Caso 5: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.THURSDAY).plusDays(7),now.with(DayOfWeek.THURSDAY).plusDays(7),false, 0);
			assertTrue(precio.toString(), precio == 5.25);
		} catch (DatoIncorrectoException e) {
			fail("Caso 6: No debería lanzar la excepción");
		}

		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(7),now.with(DayOfWeek.FRIDAY).plusDays(7),true, 450);
			assertTrue(precio.toString(), precio == 6.15);
		} catch (DatoIncorrectoException e) {
			fail("Caso 7: No debería lanzar la excepción");
		}

		// Casos de prueba no válidos
		try {
			assertTrue(CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.WEDNESDAY).plusDays(7).plusDays(1),now.with(DayOfWeek.WEDNESDAY).plusDays(7), true, 800)==1.0);			
			fail("Debería lanzar la excepcion por fecha ultima visita erronea");
		} catch (DatoIncorrectoException e) {
		}

		try {
			assertTrue(CinesUC_19.precioEntradaSocio(now.minusDays(14),now.minusDays(1), false, 999)==7.0);			
			fail("Debería lanzar la excepcion por fecha erronea");
		} catch (DatoIncorrectoException e) {				
		}

		try {
			assertTrue(CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.SUNDAY).plusDays(7).minusDays(15),now.with(DayOfWeek.SUNDAY).plusDays(7), true, -1)==7.0);			
			fail("Debería lanzar la excepcion por puntos erroneos");
		} catch (DatoIncorrectoException e) {				
		}

		
		
		//Casos caja blanca validos
		// dia mal mes bien
		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(25),now.with(DayOfWeek.FRIDAY).plusDays(7),false, 0);
			assertTrue(precio.toString(), precio == 7);
		} catch (DatoIncorrectoException e) {
			fail("Caso 8: No debería lanzar la excepción");
		}
		
		// mes y dia mal
		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(50),now.with(DayOfWeek.FRIDAY).plusDays(7),false, 0);
			assertTrue(precio.toString(), precio == 7);
		} catch (DatoIncorrectoException e) {
			fail("Caso 8: No debería lanzar la excepción");
		}
		
		// mes mal dia bien
		try {
			precio =CinesUC_19.precioEntradaSocio(now.with(DayOfWeek.FRIDAY).plusDays(7).minusDays(31),now.with(DayOfWeek.FRIDAY).plusDays(7),false, 0);
			assertTrue(precio.toString(), precio == 7);
		} catch (DatoIncorrectoException e) {
			fail("Caso 8: No debería lanzar la excepción");
		}
	}

	


}